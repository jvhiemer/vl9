/**
 * 
 */
package de.fhbingen.epro.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 * @author johanneshiemer
 *
 */
@EnableWebMvc
@ComponentScan
public class CustomMvcConfiguration {

}
